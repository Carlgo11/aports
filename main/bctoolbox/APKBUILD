# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=bctoolbox
pkgver=5.0.66
pkgrel=0
pkgdesc="Utilities library used by Belledonne Communications softwares like belle-sip, mediastreamer2 and linphone"
url="https://github.com/BelledonneCommunications/bctoolbox"
arch="all"
license="GPL-2.0-or-later"
options="!check" # bcunit not available
makedepends="cmake mbedtls-dev"
subpackages="$pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/BelledonneCommunications/bctoolbox/archive/$pkgver.tar.gz"

build() {
	cmake \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_MODULE_PATH=/usr/lib/cmake \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_SKIP_INSTALL_RPATH=ON \
		-DENABLE_MBEDTLS=YES \
		-DENABLE_POLARSSL=NO \
		-DENABLE_STATIC=NO \
		-DENABLE_TESTS_COMPONENT=OFF \
		-DENABLE_SHARED=YES .
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

dev() {
	default_dev
	mkdir -p "$subpkgdir"/usr/lib/cmake/bctoolbox
	mv "$pkgdir"/usr/share/bctoolbox/cmake/* "$subpkgdir"/usr/lib/cmake/bctoolbox
	# Remove empty dirs
	rmdir "$pkgdir"/usr/share/bctoolbox/cmake
	rmdir "$pkgdir"/usr/share/bctoolbox
	rmdir "$pkgdir"/usr/share
}

sha512sums="
4918b778a18e311e9b477574859db92a3d88b2ffb8c85081918688bf3bc104ed16bd5a5f5609d4137b3f3009ce485121fa999703d25b4867175aa1c34eb29d9f  bctoolbox-5.0.66.tar.gz
"
