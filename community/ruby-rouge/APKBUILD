# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=ruby-rouge
_gemname=rouge
pkgver=3.27.0
pkgrel=0
pkgdesc="A pure Ruby code highlighter that is compatible with Pygments"
url="https://github.com/rouge-ruby/rouge"
arch="noarch"
license="MIT AND BSD-2-Clause"
depends="ruby"
checkdepends="ruby-minitest ruby-rake"
source="https://github.com/rouge-ruby/rouge/archive/v$pkgver/$pkgname-$pkgver.tar.gz"
builddir="$srcdir/$_gemname-$pkgver"
options="!check"  # FIXME: run tests

build() {
	gem build $_gemname.gemspec
}

package() {
	local gemdir="$pkgdir/$(ruby -e 'puts Gem.default_dir')"
	local geminstdir="$gemdir/gems/$_gemname-$pkgver"

	gem install \
		--local \
		--install-dir "$gemdir" \
		--bindir "$pkgdir/usr/bin" \
		--ignore-dependencies \
		--no-document \
		--verbose \
		$_gemname

	# Remove unnecessary files and rubbish...
	cd "$gemdir"
	rm -r cache build_info doc extensions

	rm "$geminstdir"/Gemfile
}

sha512sums="
6f35c7bafa18c77a11131700fb8baac1f646be467224b9b863cafcf2445c54605c031009b14b57d95ad72b9311160e9d02e6b4a2bb2682ac4f5e88772a7cc695  ruby-rouge-3.27.0.tar.gz
"
