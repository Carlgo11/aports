# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=piping-server
pkgver=0.11.0
pkgrel=0
pkgdesc="Infinitely transfer data between devices over HTTP"
url="https://github.com/nwtgck/piping-server-rust"
license="MIT"
arch="all !s390x !mips64 !riscv64" # limited by cargo
arch="$arch !ppc64le" # FTBFS
makedepends="cargo"
subpackages="$pkgname-openrc"
source="https://github.com/nwtgck/piping-server-rust/archive/v$pkgver/piping-server-rust-$pkgver.tar.gz
	piping-server.initd
	piping-server.confd
	"
builddir="$srcdir/piping-server-rust-$pkgver"

build() {
	cargo build --release --locked
}

check() {
	cargo test --release --locked
}

package() {
	install -Dm755 target/release/piping-server "$pkgdir"/usr/bin/piping-server

	install -Dm755 "$srcdir"/$pkgname.initd "$pkgdir"/etc/init.d/$pkgname
	install -Dm644 "$srcdir"/$pkgname.confd "$pkgdir"/etc/conf.d/$pkgname
}

sha512sums="
f1c0285fde0d2d90203b45e7a6c88a6fe4d2d577f7aaece39d322ee4249119a9c1c9d55ebaf1b6163890f6489d3edb129090eb4508269e36fa1764b1d231e155  piping-server-rust-0.11.0.tar.gz
4f928fd0a0160cd3770f9c0cda5b52a3d2feb3ae2919a252d9b8e618eaf375905779853725130868b24afcb2421c5ecc894be19ce353469f696968f1a1f1ec65  piping-server.initd
250716bb0da8e61538812e233dc49d1f4a781f04ff7326475705a5c82a8df1207db8c062d4e6e9501f5cc716a4935f173e6212edfa7eddb7b02c8f26e52a5e09  piping-server.confd
"
